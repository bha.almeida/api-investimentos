package br.com.lead.investimentos.controllers;

import br.com.lead.investimentos.DTOs.SimulacaoDTO;
import br.com.lead.investimentos.models.Investimento;
import br.com.lead.investimentos.models.Simulacao;
import br.com.lead.investimentos.services.InvestimentoService;
import br.com.lead.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/investimentos")
public class InvestimentoController {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private SimulacaoService simulacaoService;

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public Investimento cadastrarInvestimento(@RequestBody @Valid Investimento investimento) {
        try {
            return investimentoService.criarInvestimento(investimento);
        } catch (RuntimeException e){
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
        }
    }

    @GetMapping
    public List<Investimento> consultarInvestimentos() {
        return investimentoService.consultarInvestimentos();
    }

    @PostMapping("/{idInvestimento}/simulacao")
    @ResponseStatus(HttpStatus.CREATED)
    public SimulacaoDTO simularAplicacao(@PathVariable(name = "idInvestimento") int idInvestimento,
                                         @RequestBody @Valid Simulacao simulacao) {
        try {
            return simulacaoService.simularAplicacao(idInvestimento, simulacao);
        } catch (RuntimeException e) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST);
        }
    }
}
