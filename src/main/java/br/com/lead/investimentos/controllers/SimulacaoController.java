package br.com.lead.investimentos.controllers;

import br.com.lead.investimentos.models.Investimento;
import br.com.lead.investimentos.models.Simulacao;
import br.com.lead.investimentos.services.SimulacaoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/simulacoes")
public class SimulacaoController {

    @Autowired
    private SimulacaoService simulacaoService;

    @GetMapping
    public List<Simulacao> consultarSimulacoes(){
        return simulacaoService.consultarSimulacoes();
    }
}
