package br.com.lead.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Digits;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Entity
public class Investimento {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome precisa ser preenchido")
    @NotBlank (message = "Nome precisa ser preenchido")
    @Column(unique = true)
    private String nome;

    @Digits(integer = 1, fraction = 2, message = "rendimento fora do padrão")
    @DecimalMin(value = "0.01", message = "rendimento deve ser ao menos 1%")
    private double rendimentoAoMes;

    public Investimento() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public double getRendimentoAoMes() {
        return rendimentoAoMes;
    }

    public void setRendimentoAoMes(double rendimentoAoMes) {
        this.rendimentoAoMes = rendimentoAoMes;
    }
}
