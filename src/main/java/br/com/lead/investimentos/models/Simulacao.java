package br.com.lead.investimentos.models;

import javax.persistence.*;
import javax.validation.constraints.*;

@Entity
public class Simulacao {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @NotNull(message = "Nome precisa ser preenchido")
    @NotBlank(message = "Nome precisa ser preenchido")
    private String nomeInteressado;

    @Email(message = "Email inválido")
    private String emailInteressado;

    @Digits(integer = 6, fraction = 2, message = "Valor da aplicação fora do padrão")
    private double valorSimulacao;

    @NotNull(message = "Quantidade de meses deve ser informada")
    @Min(value = 1, message = "Quantidade de meses deve ser maior ou igual a 1")
    private int qtdeMeses;

    @ManyToOne
    private Investimento investimento;

    public Simulacao() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNomeInteressado() {
        return nomeInteressado;
    }

    public void setNomeInteressado(String nomeInteressado) {
        this.nomeInteressado = nomeInteressado;
    }

    public String getEmailInteressado() {
        return emailInteressado;
    }

    public void setEmailInteressado(String emailInteressado) {
        this.emailInteressado = emailInteressado;
    }

    public double getValorSimulacao() {
        return valorSimulacao;
    }

    public void setValorSimulacao(double valorSimulacao) {
        this.valorSimulacao = valorSimulacao;
    }

    public int getQtdeMeses() {
        return qtdeMeses;
    }

    public void setQtdeMeses(int qtdeMeses) {
        this.qtdeMeses = qtdeMeses;
    }

    public Investimento getInvestimento() {
        return investimento;
    }

    public void setInvestimento(Investimento investimento) {
        this.investimento = investimento;
    }
}
