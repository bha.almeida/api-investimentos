package br.com.lead.investimentos.services;

import br.com.lead.investimentos.models.Investimento;
import br.com.lead.investimentos.repositories.InvestimentoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class InvestimentoService {

    @Autowired
    private InvestimentoRepository investimentoRepository;

    public Investimento criarInvestimento (Investimento investimento){
        if(!investimentoRepository.existsByNome(investimento.getNome())){
            return investimentoRepository.save(investimento);
        }
        else {
            throw new RuntimeException("Investimento já cadastrado");
        }
    }

    public List<Investimento> consultarInvestimentos (){
        return (List<Investimento>) investimentoRepository.findAll();
    }

}
