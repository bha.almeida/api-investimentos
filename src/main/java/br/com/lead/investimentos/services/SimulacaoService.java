package br.com.lead.investimentos.services;

import br.com.lead.investimentos.DTOs.SimulacaoDTO;
import br.com.lead.investimentos.models.Investimento;
import br.com.lead.investimentos.models.Simulacao;
import br.com.lead.investimentos.repositories.InvestimentoRepository;
import br.com.lead.investimentos.repositories.SimulacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class SimulacaoService {

    @Autowired
    private InvestimentoService investimentoService;

    @Autowired
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private SimulacaoRepository simulacaoRepository;

    public SimulacaoDTO simularAplicacao(int idInvestimento, Simulacao simulacao){

        SimulacaoDTO simulacaoDTO = new SimulacaoDTO();

        Optional<Investimento> investimentoOptional = investimentoRepository.findById(idInvestimento);

        if(investimentoOptional.isPresent()){
            Investimento investimentoDB = investimentoOptional.get();
            simulacao.setInvestimento(investimentoDB);
            simulacaoRepository.save(simulacao);

            double rendimentoPorMes = simulacao.getValorSimulacao() * simulacao.getInvestimento().getRendimentoAoMes();
            simulacaoDTO.setRendimentoPorMes(rendimentoPorMes);

            double montante = simulacao.getValorSimulacao() + (simulacao.getQtdeMeses() * rendimentoPorMes);
            simulacaoDTO.setMontante(montante);

            return simulacaoDTO;
        }
        else {
            throw new RuntimeException("Investimento não encontrado!");
        }
    }

    public List<Simulacao> consultarSimulacoes(){
        return (List<Simulacao>) simulacaoRepository.findAll();
    }
}
