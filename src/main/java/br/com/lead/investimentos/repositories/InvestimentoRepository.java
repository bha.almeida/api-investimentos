package br.com.lead.investimentos.repositories;

import br.com.lead.investimentos.models.Investimento;
import org.springframework.data.repository.CrudRepository;

public interface InvestimentoRepository extends CrudRepository<Investimento,Integer> {

    boolean existsByNome(String nome);

}
