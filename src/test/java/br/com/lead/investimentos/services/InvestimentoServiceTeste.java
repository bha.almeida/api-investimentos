package br.com.lead.investimentos.services;

import br.com.lead.investimentos.models.Investimento;
import br.com.lead.investimentos.repositories.InvestimentoRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;

import java.util.Arrays;
import java.util.List;

@SpringBootTest
public class InvestimentoServiceTeste {

    @MockBean
    private InvestimentoRepository investimentoRepository;

    @Autowired
    private InvestimentoService investimentoService;

    private Investimento investimento;
    private Investimento investimento2;

    @BeforeEach
    public void setUp(){
        this.investimento = new Investimento();
        investimento.setId(1);
        investimento.setNome("CDB");
        investimento.setRendimentoAoMes(0.1);

        this.investimento2 = new Investimento();
        investimento2.setId(2);
        investimento2.setNome("LCA");
        investimento2.setRendimentoAoMes(0.2);

    }

    @Test
    public void testarCadastrarInvestimento(){
        Mockito.when(investimentoRepository.existsByNome(Mockito.anyString())).thenReturn(false);
        Mockito.when(investimentoRepository.save(Mockito.any(Investimento.class))).thenReturn(this.investimento);

        Investimento investimentoTeste = investimentoService.criarInvestimento(this.investimento);
        Mockito.verify(investimentoRepository, Mockito.times(1)).save(Mockito.any(Investimento.class));
    }

    @Test
    public void testarCadastrarInvestimentoQueJaExiste(){
        Mockito.when(investimentoRepository.existsByNome(Mockito.anyString())).thenReturn(true);

        Assertions.assertThrows(RuntimeException.class, () -> {investimentoService.criarInvestimento(this.investimento);});
        Mockito.verify(investimentoRepository, Mockito.times(0)).save(Mockito.any(Investimento.class));
    }

    @Test
    public void testarConsultarInvestimentos(){
        List<Investimento> listaInvestimento = Arrays.asList(investimento, investimento2);
        Mockito.when(investimentoRepository.findAll()).thenReturn(listaInvestimento);

        investimentoService.consultarInvestimentos();
        Mockito.verify(investimentoRepository, Mockito.times(1)).findAll();
    }
}
